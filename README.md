# catalejo-electronjs

## Instalación de electronjs

```bash
nvm install node
npm init
npm install --save-dev electron
```

## Instalación de paquete

```bash
sudo apt install rpm
npm install --save-dev @electron-forge/cli
npx electron-forge import

```

## Comandos

Lanzar aplicación `npm start`

Crear paquete `npm run make` 

Regards,

Johnny Cubides
